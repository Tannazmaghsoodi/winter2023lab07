public enum Square {

  X,O,BLANK;

    public String toString() {
		if (this == Square.BLANK) {
            return "_";
        }
		
        if (this == Square.X) {
            return "X";
        }
		
        if (this == Square.O) {
            return "O";
        }
        
        return "";  
 
    }
}
