import java.util.Scanner;

public class TicTacToeGame {
  
  public static void main(String [] args){
    
    Board board = new Board(); 
    
    System.out.println("Welcome to my tic tac toe!! let's play this interesting game");
    System.out.println("Player 1's token is X  and  Player 2's token is O");
    
    boolean gameOver=false;
	
    int player = 1;
	
    Square playerToken = Square.X;
    
    Scanner keyboard = new Scanner(System.in);
    
    while(!gameOver){
		System.out.println(board);
		if(player==1){
			
			playerToken=Square.X;
			
		} else {
	
			playerToken=Square.O;
		}
    
		boolean theUsersPositionInput=false;
		
		while(!theUsersPositionInput){
			
      
			System.out.println("Player " + player +", now it's your turnto play, where do you want to place your token?(first, input your row number and then the column number)");
    
			int playerRow = (keyboard.nextInt()) -1;
			int playerColumn = (keyboard.nextInt()) -1;
    
			theUsersPositionInput = board.placeToken(playerRow, playerColumn, playerToken);
    
			if (!theUsersPositionInput){
				
				System.out.println("Your input isinvalid or blank, Please input a valid row or column number");
			}
		}   
    
		if(board.checkIfFull()){
			
			System.out.println(board); 
			System.out.println("It's a tie!");
			gameOver=true;
      
		} else if(board.checkIfWinning(playerToken)){
			
			System.out.println(board); 
			System.out.println("Congrats!!! Player "+ player+" is the winner!");
			gameOver=true;
      
		} else { 
      
			player++;
			
			if(player>2){
				player=1;
			}  
		}
    
	}
  }
}
    
    
    
    
    
    
    