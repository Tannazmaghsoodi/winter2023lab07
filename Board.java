public class Board {
  
  private Square[][] tictactoeBoard;
  
  //the Contructor that initials a field to a 3 x 3 array
  public Board(){
    tictactoeBoard = new Square[3][3];
	
    for(int i=0; i<3; i++){
      for(int j=0; j<3; j++){
		  
         tictactoeBoard[i][j] = Square.BLANK;
		 
      }
    }
  }
  
  //the Override pf tostring method
  public String toString() {
    String representationOfBoard = "";
	
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
			
            representationOfBoard += tictactoeBoard[i][j].toString() + " ";
			
        }
        representationOfBoard += "\n";
    }
    return representationOfBoard;
	
   }
 
 //instance method 
  public boolean placeToken(int row, int col, Square playerToken) {
    
    if( row>2 || row<0 || col>2 || col<0){
      return false;
    }
    
    if(tictactoeBoard[row][col] == Square.BLANK ){
      
      tictactoeBoard[row][col]=playerToken;
      return true;
	  
    }
    
    return false;
  }
      
  //instance method retures false if the square[][] has at leasr 1 blank value
  public boolean checkIfFull(){
    
    for(int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        if (tictactoeBoard[i][j] == Square.BLANK){
          return false;
        }
      }
    }
    return true;
  }
  
  //private method checks if all the squares in rows are equal to the other method (play token) 
  private boolean checkIfWinningHorizontal(Square playerToken){
    
    for(int i=0; i<3;i++){
		
      if(tictactoeBoard[i][0]==playerToken && tictactoeBoard[i][1]==playerToken && tictactoeBoard[i][2]==playerToken) {
        return true;
      }
	  
    }
	
    return false;
  }
  
  //private method checks if all the squares in columns are equal to the other method (play token)
   private boolean checkIfWinningVetical(Square playerToken){
    
    for(int j=0; j<3;j++){
		
      if(tictactoeBoard[0][j]==playerToken && tictactoeBoard[1][j]==playerToken && tictactoeBoard[2][j]==playerToken) {
        return true;
      }
	  
    }
	
    return false;
  }
   
   //bonus: checking everything diagonaly 
   private boolean checkIfWinningDiagonal( Square playerToken){
     if (tictactoeBoard[0][0] == playerToken && tictactoeBoard[1][1]== playerToken && tictactoeBoard[2][2]== playerToken){
       return true;
     }
     
      if (tictactoeBoard[0][2] == playerToken && tictactoeBoard[1][1]== playerToken && tictactoeBoard[2][0]== playerToken){
       return true;
      }
      
      return false;
   }
     
	//instance method that uses 3 private methods
   public boolean checkIfWinning (Square playerToken){
	   
     if (checkIfWinningHorizontal(playerToken) || checkIfWinningVetical(playerToken)|| checkIfWinningDiagonal(playerToken)){
       return true;
     }
	 
     return false;
   }
}






